package jp.co.icd.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserMstMaintenanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserMstMaintenanceApplication.class, args);
	}

}
