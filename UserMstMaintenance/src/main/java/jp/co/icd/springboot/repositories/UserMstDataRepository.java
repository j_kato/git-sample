package jp.co.icd.springboot.repositories;

import jp.co.icd.springboot.UserMst;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMstDataRepository extends JpaRepository<UserMst, Long>{
	
	public Optional<UserMst> findById(Long name);

}
