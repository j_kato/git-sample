package jp.co.icd.springboot;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.util.Optional;
import jp.co.icd.springboot.repositories.UserMstDataRepository;
import org.springframework.transaction.annotation.Transactional;

@Controller
public class DataController {
	
	@Autowired
	UserMstDataRepository repository;
	
	@RequestMapping("/")
	public ModelAndView top(ModelAndView mav) {
		mav.setViewName("Topage");
		return mav;
	}
	
	@RequestMapping("/index")
	public ModelAndView index(ModelAndView mav) {
		mav.setViewName("index");
		Iterable<UserMst> list = repository.findAll();
		mav.addObject("data", list);
		return mav;
	}
	
	@RequestMapping("/about")
	public ModelAndView about(ModelAndView mav) {
		mav.setViewName("about");
		mav.addObject("title","初心者による初心者のための初心者がSpringBootを完全に理解するためのアプリ");
		return mav;
	}
	
	@RequestMapping(value = "/insert", method = RequestMethod.GET)
	public ModelAndView insert(@ModelAttribute("formModel") UserMst usermst,ModelAndView mav) {
		mav.setViewName("insert");
		Iterable<UserMst> list = repository.findAll();
		mav.addObject("data",list);
		return mav;
	}
	
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@Transactional(readOnly=false)
	public ModelAndView form(
			@ModelAttribute("formModel") @Validated UserMst usermst,
			BindingResult result,
			ModelAndView mav) {
		ModelAndView res = null;
		if(!result.hasErrors()) {
			repository.saveAndFlush(usermst);
			res =  new ModelAndView("redirect:/index");
		} else {
			mav.setViewName("insert");
			mav.addObject("message","error is happened");
			Iterable<UserMst> list = repository.findAll();
			mav.addObject("data",list);
			return mav;
		}
		return res;		
	}
	
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public ModelAndView find(
			@ModelAttribute("findModel") UserMst usermst,
			ModelAndView mav ) {
		mav.setViewName("find");
		mav.addObject("message","enter id to search");
		return mav;
	}
	
	@RequestMapping(value = "/find/result", method = RequestMethod.POST)
	public ModelAndView result(
			@ModelAttribute("findModel") UserMst usermst,
			ModelAndView mav) {
		mav.setViewName("result");
		Optional<UserMst> data = repository.findById((long) usermst.getId());
		mav.addObject("data", data.get());
		return mav;
	}
	
	@RequestMapping("/posts/{id}/show")
	public ModelAndView show(
			@PathVariable int id,
			ModelAndView mav) {
		mav.setViewName("show");
		Optional<UserMst> data = repository.findById((long)id);
		mav.addObject("data", data.get());
		return mav;
	}
	
	@RequestMapping(value = "/posts/{id}/edit", method = RequestMethod.GET)
	public ModelAndView edit(
			@ModelAttribute UserMst usermst,
			@PathVariable int id,
			ModelAndView mav) {
		mav.setViewName("edit");
		Optional<UserMst> data = repository.findById((long)id);
		mav.addObject("formModel", data.get());
		return mav;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ModelAndView update(
			@ModelAttribute UserMst usermst,
			ModelAndView mav) {
		repository.saveAndFlush(usermst);
		return new ModelAndView("redirect:/index");
	}
	
	
	@RequestMapping(value= "/delete", method = RequestMethod.POST)
	@Transactional(readOnly = false)
	public ModelAndView delete(
			@RequestParam int id,
			ModelAndView mav) {
		repository.deleteById((long)id);
		return new ModelAndView("redirect:/index");
	}

}
